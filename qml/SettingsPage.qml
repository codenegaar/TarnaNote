import QtQuick 2.0
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

Item {
    property string title: "Settings"
    property alias settings: settings
    id: root
    
    Column {
        anchors.fill: parent
        spacing: 15
        
        RowLayout {
            width: parent.width
            
            Text {
                id: fontSizeText
                Layout.alignment: Qt.AlignLeft
            }
            
            Slider {
                id: fontSizeSlider
                Layout.alignment: Qt.AlignRight
                from: 3
                to: 24
                stepSize: 1
                value: settings.fontSize
                
                onMoved: {
                    settings.fontSize = value
                    fontSizeText.text = "Font size: " + value
                }
                Component.onCompleted: fontSizeText.text = "Font size: " + value
            }
        }
        
        RowLayout {
            width: parent.width
            
            Text {
                Layout.alignment: Qt.AlignLeft
                text: "Header background color"
            }
            
            Button {
                id: headerColorButton
                Layout.alignment: Qt.AlignRight
                background: Rectangle { color: settings.headerColor }
                text: "Set"
                onClicked: headerColorDialog.open()
            }
            
            ColorDialog {
                id: headerColorDialog
                title: "Choose header color"
                onAccepted: {
                    settings.headerColor = headerColorDialog.color
                }
            }
        }
    
        RowLayout {
            width: parent.width
            
            Text {
                Layout.alignment: Qt.AlignLeft
                text: "Notes' list color"
            }
            
            Button {
                id: listColorButton
                Layout.alignment: Qt.AlignRight
                background: Rectangle { color: settings.listColor }
                text: "Set"
                onClicked: listColorDialog.open()
            }
            
            ColorDialog {
                id: listColorDialog
                title: "Choose list color"
                onAccepted: {
                    settings.listColor = listColorDialog.color
                }
            }
        }
    }
    
    Settings {
        id: settings
        property real fontSize: 12
        property string headerColor: "gray"
        property string listColor: "#E5C063"
    }
}
