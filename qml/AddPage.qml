import QtQuick.Controls 2.2
import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.LocalStorage 2.0
import Qt.labs.settings 1.0
import "qrc:/js/dbManager.js" as JS

ColumnLayout {
    signal pop
    property string title: "Add a new note"
    property alias fontSize: textEdit.fontSize
    
    id: root
    
    CustomTextEdit {
        id: textEdit
        text: "Write"
        Layout.alignment: Qt.AlignTop
        Layout.fillWidth: true
        Layout.fillHeight: true
    }
    
    RowLayout {
        Layout.fillWidth: true
        Button {
            text: "Clear"
            onClicked: textEdit.text = ""
        }

        Button {
            id: button
            text: "Add"
            Layout.fillWidth: true
            
            onClicked: {
                JS.insertNote(textEdit.text)
                textEdit.text = "Write"
                root.pop()
            }
        }
    }
}
