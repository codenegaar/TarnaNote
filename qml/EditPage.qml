import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import "qrc:/js/dbManager.js" as JS

ColumnLayout {
    signal pop
    property string title: "View/Edit note"
    property alias fontSize: textEdit.fontSize
    property int noteId
    id: root
    
    CustomTextEdit {
        id: textEdit
        
        Layout.alignment: Qt.AlignTop
        Layout.fillHeight: true
        Layout.fillWidth: true
    }
    
    RowLayout {
        Layout.fillWidth: true
        
        Button {
            text: "Delete"
            Layout.fillWidth: true
            
            onClicked: {
                JS.deleteNote(noteId)
                root.pop()
            }
        }
        
        Button {
            text: "Save"
            Layout.fillWidth: true
            
            onClicked: {
                JS.updateNote(noteId, textEdit.text)
                root.pop()
            }
        }
    }
    
    function updateText() {
        textEdit.text = JS.read(noteId);
    }
}
