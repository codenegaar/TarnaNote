import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.LocalStorage 2.0
import "qrc:/js/dbManager.js" as JS

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    
    onClosing:  {
        if(stackView.depth > 1) {
            stackView.pop()
            close.accepted = false
        }
        else
            return
    }
    
    header: ToolBar {
        height: toolButton.implicitHeight
        background: Rectangle{
            color: settingsPage.settings.headerColor
        }

        ToolButton {
            id: toolButton
            anchors.left: parent.left
            anchors.top: parent.top
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            
            onClicked: {
                if(stackView.depth > 1)
                    stackView.pop()
                else
                    drawer.open()
            }
        }
        
        ToolButton {
            anchors.right: parent.right
            anchors.top: parent.top
            text: "+"
            visible: stackView.depth === 1 ? true : false
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            
            onClicked: stackView.push(addPage)
            
        }
        
        Text {
            anchors.centerIn: parent
            text: stackView.currentItem.title
        }
    }
    
    Drawer {
        id: drawer
        
        width: parent.width * 0.8
        height: parent.height
        
        Column {
            anchors.fill: parent
            Frame {
                width: parent.width
                height: parent.height / 3.5
                Image {
                    source: "qrc:/pictures/app_logo.png"
                    anchors.fill: parent
                }
            }
            
            Button {
                text: "Add a note"
                width: parent.width
                
                onClicked: {
                    stackView.push(addPage)//"qrc:/qml/AddPage.qml")
                    drawer.close()
                }
            }
            
            Button {
                width: parent.width
                text: "Settings"
                onClicked: {
                    stackView.push(settingsPage)
                    drawer.close()
                }
            }
            
            Button {
                width: parent.width
                text: "About"
                onClicked: {
                    stackView.push("qrc:/qml/AboutPage.qml")
                    drawer.close()
                }
            }
        }
    }
    
    StackView {
        id: stackView
        anchors.fill: parent
        
        initialItem: noteLister
        
        NoteLister {
            id: noteLister
            color: settingsPage.settings.listColor
            onNoteClicked: {
                editPage.noteId = noteId
                editPage.updateText()
                stackView.push(editPage)
            }
        }
        
        EditPage {
            id: editPage
            visible: false
            fontSize: settingsPage.settings.fontSize
            onPop: stackView.pop()
        }
        
        AddPage {
            id: addPage
            visible: false
            fontSize: settingsPage.settings.fontSize
            onPop: stackView.pop()
        }
        
        SettingsPage {
            visible: false
            id: settingsPage
        }
        
        onCurrentItemChanged: {
            if(depth == 1)
                noteLister.refresh();
        }
    }
    
    Component.onCompleted: JS.initialize()
}
