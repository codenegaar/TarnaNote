import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import "qrc:/js/dbManager.js" as JS

ListModel {
    
    id: listModel
    
    function refresh() {
        listModel.clear()
        JS.readAll()
    }
}
