import QtQuick 2.0
import QtQuick.Controls 2.2

Item {
    property string title: "About"
    
    
    Frame {
        id: frame
        
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        
        Image {
            id: image
            source: "qrc:/pictures/app_logo.png"
            
        }
    }
    
    Text {
        id: titleText
        
        text: "TarnaNote is a note manager"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: frame.bottom
        anchors.topMargin: 20
        
        font.pixelSize: 20
        color: "#00117F"
    }
    
    Text {
        text: "Developed by: Codenegar (Ali Rashidi)"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: titleText.bottom
        anchors.topMargin: 15
        
        font.pixelSize: 12
    }
    
    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        spacing: 5
        
        Button {
            text: "Contact on Telegram"
            onClicked: Qt.openUrlExternally("https://t.me/codenegaar")
        }
        
        Button {
            text: "Mail"
            onClicked: Qt.openUrlExternally("mailto:alirashidi127@gmail.com")
        }
    }
}
