import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.LocalStorage 2.0
import "qrc:/js/dbManager.js" as JS

ColumnLayout {
    property alias model: list.model
    property string title: "Notes' list"
    property string color
    
    signal noteClicked(int noteId)
    
    id:  root
    
    ListView {
        id: list
        implicitHeight: parent.height
        implicitWidth: parent.width
        
        clip: true
        
        model: NoteModel{
            id: noteModel
        }
        delegate: Button {
            text: model.description.length > 30 ? model.description.slice(0, 30) : model.description
            width: list.width
            height: 50
            
            onClicked: root.noteClicked(model.id)
            
            background: Rectangle {
                color: root.color
            }
        }
    }
    
    function refresh() {
        noteModel.refresh();
    }
}
