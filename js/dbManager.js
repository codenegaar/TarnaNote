
function initialize() {
    var db = LocalStorage.openDatabaseSync("NotesDatabase", "1.0", "Notes Database", 1000000);
    try {
        db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS notes (id numeric PRIMARY KEY, description text)");
        })
    } catch (err) {
        console.log("Error creating table");
    }
}

function getHandle() {
    var db;
    try {
        db = LocalStorage.openDatabaseSync("NotesDatabase", "1.0", "Notes Database", 1000000);
    } catch (err) {
        console.log("Error connecting to db");
    }
    return db;
}

function read(id) {
    var handle = getHandle();
    var description;
    handle.transaction(function(tx) {
        var result = tx.executeSql("SELECT description FROM notes WHERE id = ?;", [id]);
        description = result.rows.item(0).description.toString();
    });
    return description;
}

function readAll() {
    var handle = getHandle();
    handle.transaction(function(tx) {
        var results = tx.executeSql("SELECT * FROM notes order by id desc");
        for(var i = 0; i < results.rows.length; i++) {
            listModel.append({
                                 id: results.rows.item(i).id,
                                 description: results.rows.item(i).description
                             });
            
        }
    });
}

function insertNote(description) {
    var handle = getHandle();
    var d = new Date();
    var id = d.getSeconds() + d.getMinutes()*100 + d.getHours()*10000 + d.getDay()*1000000 + (d.getMonth() + d.getFullYear()%8)*100000000;
    try {
        handle.transaction(function(tx) {
            tx.executeSql("INSERT INTO notes (id, description) VALUES(?, ?)", [id, description]);
        });
    } catch(err) {
        console.log("Error inserting note: ", err);
    }
}

function updateNote(id, description) {
    var handle = getHandle();
    try {
        handle.transaction(function(tx){
            tx.executeSql("UPDATE notes SET description = ? WHERE id = ?", [description, id]);
        });
    } catch(err) {
        console.log("Error updating note: ", err);
    }
}

function deleteNote(id) {
    var handle = getHandle();
    try {
        handle.transaction(function(tx){
            tx.executeSql("DELETE FROM notes WHERE id = ?", [id]);
        });
    } catch(err) {
        console.log("Error deleting note: ", err);
    }
}
